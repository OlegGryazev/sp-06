package ru.gryazev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.util.DateUtils;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ProjectController {

    private final IProjectService projectService;

    private final ITaskService taskService;

    private final IUserService userService;

    @Autowired
    public ProjectController(
            final IProjectService projectService,
            final ITaskService taskService,
            final IUserService userService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @GetMapping("projects")
    public String getProjects(
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final List<Project> projects = new ArrayList<>();
        projectService.findByUserId(userEntity.getId()).forEach(o ->
                projects.add(ProjectEntity.toProjectDto(o)));
        model.put("projects", projects);
        return "project/projects";
    }

    @GetMapping("add-project")
    public String addProject(final Map<String, Object> model) {
        model.put("statuses", Status.values());
        return "project/add-project";
    }

    @PostMapping("project-remove")
    public String removeProject(
            @RequestParam("projectId") final String projectId,
            final Principal principal
    ) {
        @NotNull UserEntity userEntity = userService.findByUserName(principal.getName());
        projectService.deleteByIdAndUserId(projectId, userEntity.getId());
        return "redirect:/projects";
    }

    @PostMapping({"add-project", "edit-project"})
    public String addProject(
            @RequestParam(value = "projectId", required = false) final String projectId,
            @RequestParam("name") final String name,
            @RequestParam("details") final String details,
            @RequestParam("dateStart") final String dateStart,
            @RequestParam("dateFinish") final String dateFinish,
            @RequestParam("status") final String status,
            final Principal principal
            ) {
        @NotNull final UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final ProjectEntity project = new ProjectEntity();
        if (projectId != null) {
            project.setId(projectId);
            project.setTasks(taskService.findByProjectId(projectId));
        }
        project.setUser(userEntity);
        project.setName(name);
        project.setDetails(details);
        project.setDateStart(DateUtils.formatStringToDate(dateStart));
        project.setDateFinish(DateUtils.formatStringToDate(dateFinish));
        project.setStatus(Status.valueOf(status));
        projectService.save(project);
        return "redirect:/projects";
    }

    @GetMapping("edit-project")
    public String editProject(
            @RequestParam("projectId") final String projectId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final ProjectEntity projectEntity = projectService.findByIdAndUserId(projectId, userEntity.getId());
        model.putAll(getModelByProject(ProjectEntity.toProjectDto(projectEntity)));
        return "project/edit-project";
    }

    @GetMapping("view-project")
    public String viewProject(
            @RequestParam("projectId") final String projectId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull UserEntity userEntity = userService.findByUserName(principal.getName());
        @Nullable final ProjectEntity projectEntity = projectService.findByIdAndUserId(projectId, userEntity.getId());
        model.putAll(getModelByProject(ProjectEntity.toProjectDto(projectEntity)));
        return "project/view-project";
    }

    @NotNull
    private Map<String, Object> getModelByProject(@NotNull final Project project) {
        Map<String, Object> model = new HashMap<>();
        @Nullable final String dateStart = DateUtils.formatDateToString(project.getDateStart());
        @Nullable final String dateFinish = DateUtils.formatDateToString(project.getDateFinish());
        model.put("project", project);
        model.put("dateStart", dateStart);
        model.put("dateFinish", dateFinish);
        model.put("statuses", Status.values());
        return model;
    }

}
