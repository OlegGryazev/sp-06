package ru.gryazev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.util.DateUtils;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TaskController {

    private final IProjectService projectService;

    private final ITaskService taskService;

    private final IUserService userService;

    @Autowired
    public TaskController(
            final IProjectService projectService,
            final ITaskService taskService,
            final IUserService userService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @GetMapping("tasks")
    public String getProjects(
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final List<Task> tasks = new ArrayList<>();
        taskService.findByUserId(userEntity.getId()).forEach(o ->
                tasks.add(TaskEntity.toTaskDto(o)));
        model.put("tasks", tasks);
        return "task/tasks";
    }

    @PostMapping({"add-task", "edit-task"})
    public String addTask(
            @RequestParam("linkedProjectId") final String projectId,
            @RequestParam(value = "taskId", required = false) final String taskId,
            @RequestParam("name") final String name,
            @RequestParam("details") final String details,
            @RequestParam("dateStart") final String dateStart,
            @RequestParam("dateFinish") final String dateFinish,
            @RequestParam("status") final String status,
            final Principal principal
    ) {
        @NotNull final UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final TaskEntity task = new TaskEntity();
        if (taskId != null) {
            task.setId(taskId);
        }
        task.setProject(projectService.findById(projectId));
        task.setUser(userEntity);
        task.setName(name);
        task.setDetails(details);
        task.setDateStart(DateUtils.formatStringToDate(dateStart));
        task.setDateFinish(DateUtils.formatStringToDate(dateFinish));
        task.setStatus(Status.valueOf(status));
        taskService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("add-task")
    public String addTask(
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final List<Project> projects = new ArrayList<>();
        projectService.findByUserId(userEntity.getId()).forEach(o ->
                projects.add(ProjectEntity.toProjectDto(o)));
        model.put("projects", projects);
        model.put("statuses", Status.values());
        return "task/add-task";
    }

    @GetMapping("edit-task")
    public String editProject(
            @RequestParam("taskId") final String taskId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final TaskEntity task = taskService.findByIdAndUserId(taskId, userEntity.getId());
        @NotNull final List<Project> projects = new ArrayList<>();
        projectService.findByUserId(userEntity.getId()).forEach(o ->
                projects.add(ProjectEntity.toProjectDto(o)));
        model.put("projects", projects);
        model.putAll(getModelByTask(TaskEntity.toTaskDto(task)));
        return "task/edit-task";
    }

    @PostMapping("task-remove")
    public String removeTask(
            @RequestParam("taskId") final String taskId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final UserEntity userEntity = userService.findByUserName(principal.getName());
        taskService.deleteByIdAndUserId(taskId, userEntity.getId());
        return "redirect:/tasks";
    }

    @GetMapping("view-task")
    public String viewTask(
            @RequestParam("taskId") final String taskId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final TaskEntity taskEntity = taskService.findByIdAndUserId(taskId, userEntity.getId());
        @Nullable final Task task = TaskEntity.toTaskDto(taskEntity);
        model.putAll(getModelByTask(task));
        return "task/view-task";
    }

    private Map<String, Object> getModelByTask(final Task task) {
        Map<String, Object> model = new HashMap<>();
        @Nullable final String dateStart = DateUtils.formatDateToString(task.getDateStart());
        @Nullable final String dateFinish = DateUtils.formatDateToString(task.getDateFinish());
        model.put("task", task);
        model.put("dateStart", dateStart);
        model.put("dateFinish", dateFinish);
        model.put("statuses", Status.values());
        return model;
    }

}
