package ru.gryazev.tm.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.error.CrudNotFoundException;

import java.util.Map;

@Controller
public class RegistrationController {

    private final IUserService userService;

    public RegistrationController(final IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String login(){
        if(SecurityContextHolder.getContext().getAuthentication() != null &&
                SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
                !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)
        ) {
            return "redirect:project/projects";
        }
        return "login";
    }

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password,
            Map<String, Object> model
    ){
        try {
            userService.findByUserName(username);
        } catch (CrudNotFoundException e) {
            userService.createUser(username, password, RoleType.USER);
            return "redirect:/login";
        }
        model.put("message", "User already exists!");
        return "registration";
    }

}
