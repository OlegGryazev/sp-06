package ru.gryazev.tm.restapi;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskRESTController {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    @Autowired
    public TaskRESTController(
            final ITaskService taskService,
            final IProjectService projectService,
            final IUserService userService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @RequestMapping(value = "rest/tasks", method = RequestMethod.GET)
    public List<Task> getProjects(final Principal principal) {
        @Nullable final UserEntity userEntity = userService.findByUserName(principal.getName());
        if (userEntity == null) return null;
        @NotNull final List<Task> tasks = new ArrayList<>();
        taskService.findByUserId(userEntity.getId()).forEach(o -> tasks.add(TaskEntity.toTaskDto(o)));
        return tasks;
    }

    @RequestMapping(value = "rest/tasks/merge", method = RequestMethod.POST)
    public void addTask(@RequestBody final Task task) {
        @Nullable final TaskEntity taskEntity = Task.toTaskEntity(task, projectService, userService);
        if (taskEntity == null) return;
        taskService.save(taskEntity);
    }

    @RequestMapping(value = "rest/tasks/{taskId}", method = RequestMethod.GET)
    public Task getTask(
            @PathVariable("taskId") final String taskId,
            final Principal principal
    ) {
        @Nullable final UserEntity userEntity = userService.findByUserName(principal.getName());
        if (userEntity == null) return null;
        @Nullable final TaskEntity taskEntity = taskService.findByIdAndUserId(taskId, userEntity.getId());
        return (taskEntity == null) ? null : TaskEntity.toTaskDto(taskEntity);
    }

    @DeleteMapping("rest/tasks/delete/{taskId}")
    public void removeTask(
            @PathVariable("taskId") final String taskId,
            final Principal principal
    ) {
        @Nullable final UserEntity userEntity = userService.findByUserName(principal.getName());
        if (userEntity == null) return;
        taskService.deleteByIdAndUserId(taskId, userEntity.getId());
    }

}
