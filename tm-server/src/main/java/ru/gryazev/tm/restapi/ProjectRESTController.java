package ru.gryazev.tm.restapi;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProjectRESTController {

    private final IProjectService projectService;

    private final ITaskService taskService;

    private final IUserService userService;

    @Autowired
    public ProjectRESTController(
            final IProjectService projectService,
            final ITaskService taskService,
            final IUserService userService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @RequestMapping(value = "rest/projects", method = RequestMethod.GET)
    public List<Project> getProjects(final Principal principal) {
        @Nullable final UserEntity userEntity = userService.findByUserName(principal.getName());
        @NotNull final List<Project> projects = new ArrayList<>();
        projectService.findByUserId(userEntity.getId()).forEach(o -> projects.add(ProjectEntity.toProjectDto(o)));
        return projects;
    }

    @DeleteMapping("rest/projects/delete/{projectId}")
    public void removeProject(
            @PathVariable("projectId") final String projectId,
            final Principal principal
    ) {
        @Nullable final UserEntity userEntity = userService.findByUserName(principal.getName());
        projectService.deleteByIdAndUserId(projectId, userEntity.getId());
    }

    @RequestMapping(value = "rest/projects/merge", method = RequestMethod.POST)
    public void mergeProject(@RequestBody final Project project) {
        @Nullable ProjectEntity projectEntity = Project.toProjectEntity(project, taskService, userService);
        if (projectEntity == null) return;
        projectService.save(projectEntity);
    }

    @RequestMapping(value = "rest/projects/{projectId}", method = RequestMethod.GET)
    public Project getProject(
            @PathVariable("projectId") final String projectId,
            final Principal principal
    ) {
        @Nullable final UserEntity userEntity = userService.findByUserName(principal.getName());
        @Nullable final ProjectEntity projectEntity = projectService.findByIdAndUserId(projectId, userEntity.getId());
        return ProjectEntity.toProjectDto(projectEntity);
    }

}
