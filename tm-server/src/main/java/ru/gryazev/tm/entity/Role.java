package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.gryazev.tm.enumerated.RoleType;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "app_role")
public class Role extends AbstractCrudEntity {

    @Enumerated(EnumType.STRING)
    private RoleType role = RoleType.USER;

    @ManyToOne
    private UserEntity user;

    @Override
    public String toString() {
        return role.toString();
    }

}
