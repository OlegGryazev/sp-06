package ru.gryazev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    @NotNull private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    @Nullable
    public static Date formatStringToDate(@Nullable String dateString){
        final Date result;
        try {
            result = format.parse(dateString);
        } catch (ParseException e){
            return null;
        }
        return result;
    }

    @Nullable
    public static String formatDateToString(@Nullable Date date){
        return date == null ? null : format.format(date);
    }

}
