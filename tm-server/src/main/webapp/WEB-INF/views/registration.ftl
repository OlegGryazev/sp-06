<#import "parts/common.ftl" as c>
<@c.page>
    Add new user
    <div style="color:red">
        ${message!}
    </div>
    <form class="mt-5" action="registration" method="post">
        <div class="form-group row">
            <label for="username" class="col-sm-2 col-form-label">User</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="username" name="username" placeholder="User Name" />
            </div>
        </div>
        <div class="form-group row">
            <label for="password" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-4">
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">
                    Registration
                </button>
            </div>
        </div>
    </form>
</@c.page>