<#import "parts/common.ftl" as c>
<@c.page>
        Login page
        <form class="mt-5" action="login" method="post">
                <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">User</label>
                        <div class="col-sm-4">
                                <input type="text" class="form-control" id="username" name="username" placeholder="User Name" />
                        </div>
                </div>
                <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-4">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
                        </div>
                </div>
                <button type="submit" class="btn btn-primary">
                        Log In
                </button>
        </form>
</@c.page>