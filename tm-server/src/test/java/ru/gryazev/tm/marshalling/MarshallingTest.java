package ru.gryazev.tm.marshalling;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.gryazev.tm.config.ApplicationTestConfig;
import ru.gryazev.tm.config.ApplicationTestConfigEnabledWebMvc;
import ru.gryazev.tm.config.MockProvider;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.IUserRepository;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Optional;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationTestConfig.class, MockProvider.class, ApplicationTestConfigEnabledWebMvc.class})
public class MarshallingTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IProjectRepository projectRepository;

    private Principal mockPrincipal = Mockito.mock(Principal.class);

    private UserEntity userEntity = new UserEntity();

    private ProjectEntity projectEntity = new ProjectEntity();

    @Before
    public void setup() {
        userEntity.setUsername("test");
        initProjectEntity();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .build();
        Mockito.when(mockPrincipal.getName()).thenReturn("test");
        Mockito.when(userRepository.findByUsername("test")).thenReturn(userEntity);

        Mockito.when(projectRepository.findByIdAndUserId(projectEntity.getId(), userEntity.getId()))
                .thenReturn(Optional.of(projectEntity));
    }

    public void initProjectEntity() {
        projectEntity.setUser(userEntity);
        projectEntity.setTasks(new ArrayList<>());
        projectEntity.setName("testProjectName");
        projectEntity.setDetails("testProjectDetails");
        projectEntity.setStatus(Status.PLANNED);
    }

    @Test
    public void getProjects() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/rest/projects/" + projectEntity.getId())
                        .principal(mockPrincipal)
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(projectEntity.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(projectEntity.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userId", Matchers.is(userEntity.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.details", Matchers.is(projectEntity.getDetails())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.equalToIgnoringCase(projectEntity.getStatus().displayName())));

    }

}
