package ru.gryazev.tm.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.gryazev.tm.config.ApplicationTestConfig;
import ru.gryazev.tm.config.MockProvider;
import ru.gryazev.tm.configuration.security.UserDetailsServiceBean;
import ru.gryazev.tm.configuration.security.WebSecurityConfig;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.ITaskRepository;
import ru.gryazev.tm.repository.IUserRepository;

import java.util.Arrays;
import java.util.Optional;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        ApplicationTestConfig.class,
        MockProvider.class,
        WebSecurityConfig.class,
        UserDetailsServiceBean.class
})

public class SecurityUT {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    private UserEntity userEntity = new UserEntity();

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IProjectRepository projectRepository;

    @Autowired
    ITaskRepository taskRepository;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();

        userEntity.setUsername("test");
        Mockito.when(userRepository.findByUsername("test")).thenReturn(userEntity);
        Mockito.when(projectRepository.findByIdAndUserId("testProjectId", userEntity.getId()))
                .thenReturn(Optional.of(new ProjectEntity()));
        Mockito.when(projectRepository.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(new ProjectEntity()));
        Mockito.when(taskRepository.findByIdAndUserId("testTaskId", userEntity.getId()))
                .thenReturn(Optional.of(new TaskEntity()));
        Mockito.when(taskRepository.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(new TaskEntity()));
    }

    @Test
    public void getProjectsNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/projects"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getAddProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-project"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getEditProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/edit-project"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void PostDeleteProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.post("/project-remove"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser("test")
    public void getProjectsPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/projects"))
                .andExpect(MockMvcResultMatchers.view().name("project/projects"));
    }

    @Test
    @WithMockUser("test")
    public void getAddProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-project"))
                .andExpect(MockMvcResultMatchers.view().name("project/add-project"));
    }

    @Test
    @WithMockUser("test")
    public void getEditProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-project")
                        .param("projectId", "testProjectId")
                )
                .andExpect(MockMvcResultMatchers.view().name("project/edit-project"));
    }

    @Test
    @WithMockUser("test")
    public void PostDeleteProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/project-remove")
                        .param("projectId", "testProjectId")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/projects"));
    }

    @Test
    public void getTasksNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getAddTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-task"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getEditTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/edit-task"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void PostDeleteTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.post("/task-remove"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser("test")
    public void getTasksPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(MockMvcResultMatchers.view().name("task/tasks"));
    }

    @Test
    @WithMockUser("test")
    public void getAddTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-task"))
                .andExpect(MockMvcResultMatchers.view().name("task/add-task"));
    }

    @Test
    @WithMockUser("test")
    public void getEditTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-task")
                        .param("taskId", "testTaskId")
                )
                .andExpect(MockMvcResultMatchers.view().name("task/edit-task"));
    }

    @Test
    @WithMockUser("test")
    public void PostDeleteTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/task-remove")
                        .param("taskId", "testTaskId")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/tasks"));
    }

}
