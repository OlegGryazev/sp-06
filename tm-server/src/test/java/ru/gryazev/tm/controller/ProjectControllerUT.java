package ru.gryazev.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.gryazev.tm.config.ApplicationTestConfig;
import ru.gryazev.tm.config.MockProvider;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.IUserRepository;

import java.security.Principal;
import java.util.Arrays;
import java.util.Optional;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationTestConfig.class, MockProvider.class})
public class ProjectControllerUT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IProjectRepository projectRepository;

    private Principal mockPrincipal = Mockito.mock(Principal.class);

    private UserEntity userEntity = new UserEntity();

    @Before
    public void setup() {
        userEntity.setUsername("test");
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .build();
        Mockito.when(mockPrincipal.getName()).thenReturn("test");
        Mockito.when(userRepository.findByUsername("test")).thenReturn(userEntity);
        Mockito.when(projectRepository.findByIdAndUserId("testProjectId", userEntity.getId()))
                .thenReturn(Optional.of(new ProjectEntity()));
        Mockito.when(projectRepository.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(new ProjectEntity()));
    }

    @Test
    public void getProjects() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/projects").principal(mockPrincipal))
                .andExpect(MockMvcResultMatchers.view().name("project/projects"));
    }

    @Test
    public void getAddProject() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-project").principal(mockPrincipal))
                .andExpect(MockMvcResultMatchers.view().name("project/add-project"));
    }

    @Test
    public void getEditProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-project")
                        .param("projectId", "testProjectId")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("project/edit-project"));
    }

    @Test
    public void getEditProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-project")
                        .param("projectId", "not_found")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getViewProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/view-project")
                        .param("projectId", "testProjectId")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("project/view-project"));
    }

    @Test
    public void getViewProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/view-project")
                        .param("projectId", "not_found")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void postDeleteProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/project-remove")
                        .param("projectId", "testProjectId")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/projects"));
    }

    @Test
    public void postAddProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/add-project")
                        .param("name", "test-name")
                        .param("details", "details")
                        .param("dateStart", "01-01-2020")
                        .param("dateFinish", "01-01-2020")
                        .param("status", "PLANNED")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/projects"));
    }

    @Test
    public void postEditProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/edit-project")
                        .param("projectId", "testProjectId")
                        .param("name", "test-name")
                        .param("details", "details")
                        .param("dateStart", "01-01-2020")
                        .param("dateFinish", "01-01-2020")
                        .param("status", "PLANNED")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/projects"));
    }

}