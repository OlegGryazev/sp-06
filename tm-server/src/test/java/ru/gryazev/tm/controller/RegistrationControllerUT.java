package ru.gryazev.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.gryazev.tm.config.ApplicationTestConfig;
import ru.gryazev.tm.config.MockProvider;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.repository.IUserRepository;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationTestConfig.class, MockProvider.class})
public class RegistrationControllerUT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserRepository userRepository;

    private UserEntity userEntity = new UserEntity();

    @Before
    public void setup() {
        userEntity.setUsername("test");
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .build();
        Mockito.when(userRepository.findByUsername("test")).thenReturn(userEntity);
    }

    @Test
    public void getLogin() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(MockMvcResultMatchers.view().name("login"));
    }

    @Test
    public void getRegistration() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/registration"))
                .andExpect(MockMvcResultMatchers.view().name("registration"));
    }

    @Test
    public void postRegistration() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/registration")
                        .param("username", "newUser")
                        .param("password", "123")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/login"));
    }

    @Test
    public void postRegistrationNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/registration")
                        .param("username", "test")
                        .param("password", "123")
                )
                .andExpect(MockMvcResultMatchers.view().name("registration"));
    }

}