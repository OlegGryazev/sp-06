package ru.gryazev.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.gryazev.tm.config.ApplicationTestConfig;
import ru.gryazev.tm.config.MockProvider;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.ITaskRepository;
import ru.gryazev.tm.repository.IUserRepository;

import java.security.Principal;
import java.util.Arrays;
import java.util.Optional;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationTestConfig.class, MockProvider.class})
public class TaskControllerUT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    private Principal mockPrincipal = Mockito.mock(Principal.class);

    private UserEntity userEntity = new UserEntity();

    @Before
    public void setup() {
        userEntity.setUsername("test");
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .build();
        Mockito.when(mockPrincipal.getName()).thenReturn("test");
        Mockito.when(userRepository.findByUsername("test")).thenReturn(userEntity);
        Mockito.when(taskRepository.findByIdAndUserId("testTaskId", userEntity.getId()))
                .thenReturn(Optional.of(new TaskEntity()));
        Mockito.when(taskRepository.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(new TaskEntity()));
        Mockito.when(projectRepository.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(new ProjectEntity()));
        Mockito.when(projectRepository.findById("linkedProjectId"))
                .thenReturn(Optional.of(new ProjectEntity()));
    }

    @Test
    public void getTasks() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/tasks").principal(mockPrincipal))
                .andExpect(MockMvcResultMatchers.view().name("task/tasks"));
    }

    @Test
    public void getAddTask() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-task").principal(mockPrincipal))
                .andExpect(MockMvcResultMatchers.view().name("task/add-task"));
    }

    @Test
    public void getEditTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-task")
                        .param("taskId", "testTaskId")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("task/edit-task"));
    }

    @Test
    public void getEditTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-task")
                        .param("taskId", "not_found")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getViewTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/view-task")
                        .param("taskId", "testTaskId")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("task/view-task"));
    }

    @Test
    public void getViewTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/view-task")
                        .param("taskId", "not_found")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void postDeleteTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/task-remove")
                        .param("taskId", "testTaskId")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/tasks"));
    }

    @Test
    public void postAddTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/add-task")
                        .param("linkedProjectId", "linkedProjectId")
                        .param("name", "test-name")
                        .param("details", "details")
                        .param("dateStart", "01-01-2020")
                        .param("dateFinish", "01-01-2020")
                        .param("status", "PLANNED")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/tasks"));
    }

    @Test
    public void postEditProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/edit-task")
                        .param("linkedProjectId", "linkedProjectId")
                        .param("taskId", "testTaskId")
                        .param("name", "test-name")
                        .param("details", "details")
                        .param("dateStart", "01-01-2020")
                        .param("dateFinish", "01-01-2020")
                        .param("status", "PLANNED")
                        .principal(mockPrincipal)
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/tasks"));
    }

}