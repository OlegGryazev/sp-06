package ru.gryazev.tm.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.configuration.ApplicationConfig;
import ru.gryazev.tm.configuration.security.WebSecurityConfig;
import ru.gryazev.tm.controller.ProjectController;
import ru.gryazev.tm.controller.RegistrationController;
import ru.gryazev.tm.controller.TaskController;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.ITaskRepository;
import ru.gryazev.tm.repository.IUserRepository;
import ru.gryazev.tm.restapi.ProjectRESTController;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.service.UserService;

import java.util.List;

@Configuration
@ContextConfiguration(classes = {WebSecurityConfig.class, MockProvider.class, ApplicationConfig.class})
public class ApplicationTestConfig implements WebMvcConfigurer {

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public IUserService userService(IUserRepository userRepository, PasswordEncoder passwordEncoder) {
        return new UserService(userRepository, passwordEncoder);
    }

    @Bean
    public IProjectService projectService(IProjectRepository projectRepository) {
        return new ProjectService(projectRepository);
    }

    @Bean
    public ITaskService taskService(ITaskRepository taskRepository) {
        return new TaskService(taskRepository);
    }

    @Bean
    public ProjectController projectController(
            IProjectService projectService,
            ITaskService taskService,
            IUserService userService
    ) {
        return new ProjectController(projectService, taskService, userService);
    }

    @Bean
    public TaskController taskController(
            IProjectService projectService,
            ITaskService taskService,
            IUserService userService
    ) {
        return new TaskController(projectService, taskService, userService);
    }

    @Bean
    public RegistrationController registrationController(IUserService userService) {
        return new RegistrationController(userService);
    }

    @Bean
    public ProjectRESTController projectRESTController(
            IProjectService projectService,
            ITaskService taskService,
            IUserService userService
    ) {
        return new ProjectRESTController(projectService, taskService, userService);
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializationInclusion(JsonInclude.Include.NON_NULL);
        builder.serializationInclusion(JsonInclude.Include.NON_EMPTY);
        builder.indentOutput(true).dateFormat(new StdDateFormat().withColonInTimeZone(true));
        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
    }

}
