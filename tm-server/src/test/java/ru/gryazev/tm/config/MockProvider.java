package ru.gryazev.tm.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.ITaskRepository;
import ru.gryazev.tm.repository.IUserRepository;

@Configuration
public class MockProvider {

    @Bean
    public IUserRepository userRepository() {
        return  Mockito.mock(IUserRepository.class);
    }

    @Bean
    public IProjectRepository projectRepository() {
        return  Mockito.mock(IProjectRepository.class);
    }

    @Bean
    public ITaskRepository taskRepository() {
        return  Mockito.mock(ITaskRepository.class);
    }

}
