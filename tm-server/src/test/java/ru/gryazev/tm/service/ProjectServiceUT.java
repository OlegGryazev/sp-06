package ru.gryazev.tm.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.config.ApplicationTestConfig;
import ru.gryazev.tm.config.MockProvider;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.repository.IProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationTestConfig.class, MockProvider.class})
public class ProjectServiceUT {

    @Autowired
    IProjectRepository projectRepository;

    @Autowired
    IProjectService projectService;

    ProjectEntity projectEntity;

    @Before
    public void initMock() {
        projectEntity = new ProjectEntity();
        projectEntity.setUser(new UserEntity());
        List<ProjectEntity> projectEntities = new ArrayList<>();
        projectEntities.add(projectEntity);
        projectEntity.setName("name");
        Mockito.when(projectRepository.findById("id")).thenReturn(Optional.of(projectEntity));
        Mockito.when(projectRepository.findByIdAndUserId("id", "id")).thenReturn(Optional.of(projectEntity));
        Mockito.when(projectRepository.findByUserId("id")).thenReturn(projectEntities);
        Mockito.when(projectRepository.save(projectEntity)).thenReturn(projectEntity);
    }

    @Test
    public void projectFindByIdPositive() {
        assertEquals("name", projectService.findById("id").getName());
    }

    @Test(expected = CrudNotFoundException.class)
    public void projectFindByIdNullNegative() {
        projectService.findById(null);
    }

    @Test(expected = CrudNotFoundException.class)
    public void projectFindByIdEmptyNegative() {
        projectService.findById("");
    }

    @Test(expected = CrudNotFoundException.class)
    public void projectFindByIdUnknownNegative() {
        projectService.findById("not_found");
    }

    @Test(expected = CrudNotFoundException.class)
    public void projectFindByIdAndUserIdAllNullNegative() {
        projectService.findByIdAndUserId(null, null);
    }

    @Test(expected = CrudNotFoundException.class)
    public void projectFindByIdAndUserIdAllEmptyNegative() {
        projectService.findByIdAndUserId("", "");
    }

    @Test(expected = CrudNotFoundException.class)
    public void projectFindByIdAndUserIdUnknownNegative() {
        projectService.findByIdAndUserId("not_found", "not_found");
    }

    @Test
    public void projectFindByIdAndUserIdPositive() {
        assertEquals("name", projectService.findByIdAndUserId("id", "id").getName());
    }

    @Test
    public void projectFindByUserIdNegative() {
        assertEquals(0, projectService.findByUserId(null).size());
        assertEquals(0, projectService.findByUserId("not_found").size());
        assertEquals(0, projectService.findByUserId("").size());
    }

    @Test
    public void projectFindByUserIdPositive() {
        assertEquals(1, projectService.findByUserId("id").size());
    }

    @Test
    public void projectSavePositive() {
        assertNotNull(projectService.save(projectEntity));
    }

    @Test
    public void projectSaveNegative() {
        assertNull(projectService.save(null));
        assertNull(projectService.save(new ProjectEntity()));
    }

    @Test
    public void projectDeleteByIdAndUserIdPositive() {
        projectService.deleteByIdAndUserId("id", "id");
    }

    @Test(expected = CrudNotFoundException.class)
    public void projectDeleteByIdAndUserIdAllNullNegative() {
        projectService.deleteByIdAndUserId(null, null);
    }

    @Test(expected = CrudNotFoundException.class)
    public void projectDeleteByIdAndUserIdAllEmptyNegative() {
        projectService.deleteByIdAndUserId("", "");
    }

}
