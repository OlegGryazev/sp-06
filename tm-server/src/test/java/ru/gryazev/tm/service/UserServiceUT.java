package ru.gryazev.tm.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.config.ApplicationTestConfig;
import ru.gryazev.tm.config.MockProvider;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.repository.IUserRepository;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationTestConfig.class, MockProvider.class})
public class UserServiceUT {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IUserService userService;

    private UserEntity userEntity;

    @Before
    public void initMock() {
        userEntity = new UserEntity();
        userEntity.setUsername("username");
        userEntity.setPassword("password");
        Mockito.when(userRepository.findByUsername("name")).thenReturn(userEntity);
        Mockito.when(userRepository.findById("id")).thenReturn(Optional.of(userEntity));
        Mockito.when(userRepository.existsById("id")).thenReturn(true);
    }

//    @Test
//    public void createUserPositive() {
//        assertNotNull(userService.createUser("username", "password"));
//    }

    @Test
    public void createUserNegative() {
        assertNull(userService.createUser("", "password"));
        assertNull(userService.createUser("username", ""));
        assertNull(userService.createUser(null, "password"));
        assertNull(userService.createUser("username", null));
    }

    @Test
    public void findByUserNamePositive() {
        assertNotNull(userService.findByUserName("name"));
    }

    @Test(expected = CrudNotFoundException.class)
    public void findByUserNameNullNegative() {
        userService.findByUserName(null);
    }

    @Test(expected = CrudNotFoundException.class)
    public void findByUserNameEmptyNegative() {
        userService.findByUserName("");
    }

    @Test(expected = CrudNotFoundException.class)
    public void findByUserNameNotFoundNegative() {
        userService.findByUserName("not_found");
    }

    @Test
    public void findByIdPositive() {
        assertNotNull(userService.findById("id"));
    }

    @Test(expected = CrudNotFoundException.class)
    public void findByIdNullNegative() {
        userService.findById(null);
    }

    @Test(expected = CrudNotFoundException.class)
    public void findByIdEmptyNegative() {
        userService.findById("");
    }

    @Test(expected = CrudNotFoundException.class)
    public void findByIdNotFoundNegative() {
        userService.findById("not_found");
    }

    @Test
    public void deleteByIdPositive() {
        userService.deleteById("id");
    }

    @Test(expected = CrudNotFoundException.class)
    public void deleteByIdNullNegative() {
        userService.deleteById(null);
    }

    @Test(expected = CrudNotFoundException.class)
    public void deleteByIdEmptyNegative() {
        userService.deleteById("");
    }

    @Test(expected = CrudNotFoundException.class)
    public void deleteByIdNotFoundNegative() {
        userService.deleteById("not_found");
    }

}
