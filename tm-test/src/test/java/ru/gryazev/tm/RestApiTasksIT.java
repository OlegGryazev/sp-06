package ru.gryazev.tm;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.entity.User;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RestApiTasksIT {

    private static Task task = new Task();

    private static User user = new User();

    private static RequestSpecification request;

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost/projectmanager/rest";
        RestAssured.port = 8080;
        user.setUsername("rest-assured-test-user");
        user.setPassword("1");
        userRegistration();
        final String userId = given()
                .auth().basic(user.getUsername(), user.getPassword())
                .get("/users/" + user.getUsername()).getBody().jsonPath().getString("id");
        user.setId(userId);
        task.setName("assured-test-task");
        task.setUserId(user.getId());
        request = RestAssured.given()
                .auth().basic(user.getUsername(), user.getPassword())
                .contentType(ContentType.JSON);
    }

    @AfterClass
    public static void clear() {
        request.delete("/users/delete/self");
    }

    public static void userRegistration() {
        final Map<String, String> requestBody = new HashMap<>();
        requestBody.put("username", user.getUsername());
        requestBody.put("password", user.getPassword());
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when().post("/users/registration")
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void testAddGetDeleteTaskOk() {
        request
                .body(task)
                .when().post("/tasks/merge")
                .then().statusCode(HttpStatus.OK.value());

        request
                .when().get("/tasks/" + task.getId())
                .then().body("name", equalTo("assured-test-task"));

        request
                .when().delete("/tasks/delete/" + task.getId())
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void testAddTask() {
        final String responseBefore = request.get("/tasks").asString();
        final int tasksCountBefore = JsonPath.from(responseBefore).getList("").size();
        request
                .body(task)
                .when().post("/tasks/merge")
                .then().statusCode(HttpStatus.OK.value());
        final String responseAfter = request.get("/tasks").asString();
        final int tasksCountAfter = JsonPath.from(responseAfter).getList("").size();
        assertEquals(tasksCountAfter, tasksCountBefore + 1);
    }

    @Test
    public void testViewTaskNotFound() {
        final String response = request.get("/tasks/test-assured-id").asString();
        assertTrue(response.isEmpty());
    }

    @Test
    public void testMethodIsNotAllowed() {
        request
                .when().delete("/tasks/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        request
                .when().post("/tasks")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        request
                .when().post("/tasks/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        request
                .when().get("/tasks/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        request
                .when().delete("/tasks/add" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());
    }

}
