package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Setter
@Getter
public abstract class AbstractCrudDto {

    @NotNull
    private String id = UUID.randomUUID().toString();

}
