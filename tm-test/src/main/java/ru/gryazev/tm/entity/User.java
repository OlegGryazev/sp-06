package ru.gryazev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends AbstractCrudDto {

    @Nullable
    private String username;

    @Nullable
    private String password;

}
